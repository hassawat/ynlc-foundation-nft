require('dotenv').config();
const ethers = require('ethers');

// Define an Alchemy Provider
const provider = new ethers.providers.AlchemyProvider('goerli', process.env.API_KEY);

// Get contract ABI file
const contract = require("../artifacts/contracts/YNLC.sol/YOLONEKOCLUB.json");

// Create a signer
const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider)

// Create a contract instance
const NFTContract = new ethers.Contract(process.env.CONTRACT_ADDRESS, contract.abi, signer);

// Call mintNFT function
const mintNFT = async (tokenURI) => {
	let NFTTxn = await NFTContract.safeMint(signer.address, tokenURI)
	await NFTTxn.wait()
	console.log(`NFT Minted! Check it out at: https://goerli.etherscan.io/tx/${NFTTxn.hash}`)
}

mintNFT('https://gateway.pinata.cloud/ipfs/QmYueiuRNmL4MiA2GwtVMm6ZagknXnSpQnB3z2gWbz36hP')
	.then(() => process.exit(0))
	.catch((error) => {
		console.error(error);
		process.exit(1);
	});