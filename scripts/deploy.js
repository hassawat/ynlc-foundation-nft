async function main() {

	const [deployer] = await ethers.getSigners();
	console.log("Deploying contracts with the account:", deployer.address);
	console.log("Account balance:", (await deployer.getBalance()).toString());

	const YNLC = await ethers.getContractFactory("YOLONEKOCLUB")

	// Start deployment, returning a promise that resolves to a contract object
	const NFTs = await YNLC.deploy()
	await NFTs.deployed()

	console.log("Contract deployed to address:", NFTs.address)
}

main().then(() => process.exit(0)).catch((error) => {
	console.error('error', error)
	process.exit(1)
})