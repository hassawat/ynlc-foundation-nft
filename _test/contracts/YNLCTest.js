const { expect } = require("chai");

describe("YNLC contract", function () {
	it("Deployment should assign the total supply of tokens to the owner", async function () {
		const [deployer] = await ethers.getSigners();

		const NFTs = await ethers.getContractFactory("YOLONEKOCLUB");

		const hardhatNFTs = await NFTs.deploy();

		const deployerBalance = await hardhatNFTs.balanceOf(deployer.address);
		expect(await hardhatNFTs.totalSupply()).to.equal(deployerBalance);
	});
});