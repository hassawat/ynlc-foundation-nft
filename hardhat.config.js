/** @type import('hardhat/config').HardhatUserConfig */

require('dotenv').config();
require("@nomiclabs/hardhat-ethers");
require("@nomiclabs/hardhat-etherscan");

module.exports = {
	solidity: "0.8.17",
	defaultNetwork: "goerli",
	networks: {
		hardhat: {},
		goerli: {
			url: `${process.env.API_URL}/${process.env.API_KEY}`,
			accounts: [`0x${process.env.PRIVATE_KEY}`]
		}
	},
	etherscan: {
		// Your API key for Etherscan
		// Obtain one at https://etherscan.io/
		apiKey: process.env.ETHERSCAN_API_KEY
	}
};